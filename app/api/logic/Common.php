<?php
// +----------------------------------------------------------------------
// | Author: Zaker <49007623@qq.com>
// +----------------------------------------------------------------------

namespace app\api\logic;

use app\api\error\CodeBase;
use app\api\error\Common as CommonError;
use app\common\logic\User as LogicMember;
use \extend\JWT\JWT;
use app\common\logic\Common as LogicCommon;
use app\common\logic\File as LogicFile;


/**
 * 接口基础逻辑
 */
class Common extends ApiBase
{

    /**
     * 登录接口逻辑
     */
    public static function login($data = [])
    {
      
    	$commonLogic = get_sington_object('commonLogic', LogicCommon::class,'user');
    	 
    	 
    	$result = $commonLogic->getDataInfo(['moble'=>$data['username']]);
    	
    	if(empty($result)){
    		
    		
    		$ret['status']='error';
    		$ret['msg']='尚未登记该手机号';
    		 
    		return [$ret];
    	}
    	
    	unset($data['access_token']);
    	
    	if(!empty($data['safe'])){
    		if($data['safe']==1){
    			 
    			$data['password']=$pass=utf8RawUrlDecode(base64_decode($data['password']));
    			 
    		}
    		unset($data['safe']);
    	}

    	
        $validate = validate('User');
        
        $validate_result = $validate->scene('login')->check($data);
        
        if (!$validate_result) : return CommonError::$usernameOrPasswordEmpty; endif;
        
        $memberLogic = get_sington_object('memberLogic', LogicMember::class);
        
        begin:
        
        $member = $memberLogic->getMemberInfo(['username' => $data['username']]);

        // 若不存在用户则注册
        if (empty($member))
        {
        	$data['nickname']  = $result['username'];
        	
            $register_result = static::register($data);
            
            if (!$register_result) : return CommonError::$registerFail; endif;
            
            goto begin;
        }
        
        if (md5($data['password'].$member['salt']) !== $member['password']) : return CommonError::$passwordError; endif;
        
        $jwt_data = static::tokenSign($member);
        
        return [$jwt_data];
    }
    
    public static function register($data)
    {
        
        
        $salt = generate_password(18);
        $data['salt'] = $salt;
        $data['password'] = md5($data['password'].$salt);
       $data['mobile']  = $data['username'];
        $data['regtime'] = time();
        $data['usermail'] = $salt.'@qq.com';
       // $data['password']  = data_md5_key($data['password']);

        $memberLogic = get_sington_object('memberLogic', LogicMember::class);
        
        return $memberLogic->setInfo($data);
    }
    
    public static function tokenSign($member)
    {
        
        $key = API_KEY . JWT_KEY;
        
        $jwt_data = ['member_id' => $member['id'], 'nickname' => $member['nickname'], 'username' => $member['username'], 'regtime' => $member['regtime']];
        
        $token = [
            "iss"   => "EasySNS JWT",         // 签发者
            "iat"   => TIME_NOW,              // 签发时间
            "exp"   => TIME_NOW +7200,   // 过期时间
            "aud"   => 'EasySNS',             // 接收方
            "sub"   => 'EasySNS',             // 面向的用户
            "data"  => $jwt_data
        ];
        
        $jwt = JWT::encode($token, $key);
        
        $jwt_data['user_token'] = $jwt;
        
        return $jwt_data;
    }
  
  
}
