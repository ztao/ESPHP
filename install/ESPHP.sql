﻿

DROP TABLE IF EXISTS `es_usercz`;
CREATE TABLE `es_usercz` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) unsigned NOT NULL COMMENT '用户id',
  `did` int(11) unsigned NOT NULL COMMENT '目标id',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '数据状态',
  `cid` tinyint(1) NOT NULL DEFAULT '1' COMMENT '内容类型1为帖子2小组3用户',
  `type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1为下载2为浏览',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='用户操作';





DROP TABLE IF EXISTS `es_action_log`;
CREATE TABLE `es_action_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `member_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '执行会员id',
  `username` char(30) NOT NULL DEFAULT '' COMMENT '用户名',
  `ip` char(30) NOT NULL DEFAULT '' COMMENT '执行行为者ip',
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '行为名称',
  `describe` varchar(255) NOT NULL DEFAULT '' COMMENT '描述',
  `url` varchar(255) NOT NULL DEFAULT '' COMMENT '执行的URL',
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '状态',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '执行行为的时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED COMMENT='行为日志表';


DROP TABLE IF EXISTS `es_api`;
CREATE TABLE `es_api` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` char(150) NOT NULL DEFAULT '' COMMENT '接口名称',
  `group_id` int(6) unsigned NOT NULL DEFAULT '0' COMMENT '接口分组',
  `request_type` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '请求类型 0:POST  1:GET',
  `api_url` char(50) NOT NULL DEFAULT '' COMMENT '请求路径',
  `describe` varchar(255) NOT NULL DEFAULT '' COMMENT '接口描述',
  `describe_text` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '接口富文本描述',
  `is_request_data` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否需要请求数据',
  `request_data` text NOT NULL COMMENT '请求数据',
  `response_data` text NOT NULL COMMENT '响应数据',
  `is_response_data` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否需要响应数据',
  `is_user_token` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否需要用户token',
  `is_response_sign` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否返回数据签名',
  `is_request_sign` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否验证请求数据签名',
  `response_examples` text NOT NULL COMMENT '响应栗子',
  `developer` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '研发者',
  `api_status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '接口状态（0:待研发，1:研发中，2:测试中，3:已完成）',
  `is_page` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否为分页接口 0：否  1：是',
  `sort` tinyint(5) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '数据状态',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=192 DEFAULT CHARSET=utf8 COMMENT='API表';


DROP TABLE IF EXISTS `es_api_group`;
CREATE TABLE `es_api_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` char(120) NOT NULL DEFAULT '' COMMENT 'aip分组名称',
  `sort` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='api分组表';


INSERT INTO `es_api_group` VALUES ('1', '基础接口', '0', '1504501195', '0', '1');
INSERT INTO `es_api_group` VALUES ('2', '聚合接口', '0', '1504784149', '1504784149', '1');


DROP TABLE IF EXISTS `es_picture`;
CREATE TABLE `es_picture` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id自增',
  `name` varchar(255) NOT NULL DEFAULT '' COMMENT '图片名称',
  `path` varchar(255) NOT NULL DEFAULT '' COMMENT '路径',
  `url` varchar(255) NOT NULL DEFAULT '' COMMENT '图片链接',
  `sha1` char(40) NOT NULL DEFAULT '' COMMENT '文件 sha1编码',
  `md5` char(32) NOT NULL DEFAULT '' COMMENT '文件md5',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='图片表';

DROP TABLE IF EXISTS `es_driver`;
CREATE TABLE `es_driver` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `service_name` varchar(40) NOT NULL DEFAULT '' COMMENT '服务标识',
  `driver_name` varchar(20) NOT NULL DEFAULT '' COMMENT '驱动标识',
  `config` text NOT NULL COMMENT '配置',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '安装时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=33 DEFAULT CHARSET=utf8 COMMENT='插件表';

DROP TABLE IF EXISTS `es_file`;
CREATE TABLE `es_file` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '文件ID',
  `name` varchar(100) NOT NULL DEFAULT '' COMMENT '原始文件名',
  `savename` varchar(100) NOT NULL DEFAULT '' COMMENT '保存名称',
  `savepath` varchar(255) NOT NULL DEFAULT '' COMMENT '文件保存路径',
  `ext` char(10) NOT NULL DEFAULT '' COMMENT '文件后缀',
  `mime` char(50) NOT NULL DEFAULT '' COMMENT '文件mime类型',
  `size` int(15) unsigned NOT NULL DEFAULT '0' COMMENT '文件大小',
  `sha1` char(40) NOT NULL DEFAULT '' COMMENT '文件 sha1编码',
  `md5` char(32) NOT NULL DEFAULT '' COMMENT '文件md5',
  `url` varchar(255) NOT NULL DEFAULT '' COMMENT '远程地址',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '上传时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='文件表';



DROP TABLE IF EXISTS `es_hook`;
CREATE TABLE `es_hook` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(40) NOT NULL DEFAULT '' COMMENT '钩子名称',
  `describe` varchar(255) NOT NULL COMMENT '描述',
  `addon_list` varchar(255) NOT NULL DEFAULT '' COMMENT '钩子挂载的插件 ''，''分割',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=30 DEFAULT CHARSET=utf8 COMMENT='钩子表';


INSERT INTO `es_hook` VALUES ('28', 'ArticleEditor', '富文本编辑器', 'Editor', '1', '0', '0');
INSERT INTO `es_hook` VALUES ('29', 'ImgUpload', '图片上传钩子', 'Imginput', '1', '0', '0');

DROP TABLE IF EXISTS `es_addon`;
CREATE TABLE `es_addon` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(40) NOT NULL DEFAULT '' COMMENT '插件名或标识',
  `title` varchar(20) NOT NULL DEFAULT '' COMMENT '中文名称',
  `describe` varchar(255) NOT NULL DEFAULT '' COMMENT '插件描述',
  `config` text NOT NULL COMMENT '配置',
  `author` varchar(40) NOT NULL DEFAULT '' COMMENT '作者',
  `version` varchar(20) NOT NULL DEFAULT '' COMMENT '版本号',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态',
  `has_adminlist` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否有后台列表',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '安装时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 COMMENT='插件表';

INSERT INTO `es_addon` VALUES(27, 'Editor', '文本编辑器', '富文本编辑器', '{"editor_resize_type":"1","editor_height":"300px"}', 'Bigotry', '1.0', 1, 0, 1502868284, 1502868284);
INSERT INTO `es_addon` VALUES(28, 'Imginput', '图片上传', '图片上传插件，可支持拖动图片及批量上传', '', 'Bigotry', '1.0', 1, 0, 1502868284, 1502868284);


DROP TABLE IF EXISTS `es_usergrade`;
CREATE TABLE IF NOT EXISTS `es_usergrade` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL COMMENT '名称',
  `score` int(11) NOT NULL COMMENT '等级所需积分',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '安装时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='会员等级表';

DROP TABLE IF EXISTS `es_user`;
CREATE TABLE IF NOT EXISTS `es_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `point` int(11) NOT NULL DEFAULT '0' COMMENT '积分',
  `expoint1` int(11) DEFAULT '0' COMMENT '扩展积分1',
  `expoint2` int(11) DEFAULT '0' COMMENT '扩展积分2',
  `expoint3` int(11) DEFAULT '0' COMMENT '扩展积分3',
  `userip` varchar(32) NOT NULL COMMENT 'IP',
  `nickname` char(50) NOT NULL DEFAULT '' COMMENT '昵称',
  `username` varchar(32) NOT NULL COMMENT '名称',
  `password` varchar(32) NOT NULL COMMENT '密码',
  `userhead` varchar(100) DEFAULT '/public/images/default.png' COMMENT '头像',
  `usermail` varchar(32) NOT NULL COMMENT '邮箱',
  `mobile` varchar(11) DEFAULT '' COMMENT '手机',
  `regtime` varchar(32) NOT NULL COMMENT '注册时间',
  `grades` tinyint(1) NOT NULL DEFAULT '0' COMMENT '等级',
  `sex` tinyint(1) NOT NULL DEFAULT '0' COMMENT '性别',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '验证1表示正常2邮箱验证3手机认证5手机邮箱全部认证',
  `statusdes` varchar(200) DEFAULT NULL COMMENT '认证描述',
  `userhome` varchar(32) DEFAULT NULL COMMENT '家乡',
  `description` varchar(200) DEFAULT NULL COMMENT '描述',
  `last_login_time` varchar(20) DEFAULT '0' COMMENT '最后登陆时间',
  `last_login_ip` varchar(50) DEFAULT '' COMMENT '最后登录IP',
  `salt` varchar(20) DEFAULT NULL COMMENT 'salt',
  `leader_id` int(10) unsigned NOT NULL DEFAULT '1' COMMENT '上级会员ID',
  `is_share_member` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否共享会员',
  `is_inside` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否为后台使用者',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`) USING BTREE,
  UNIQUE KEY `usermail` (`usermail`) USING BTREE
)ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='用户表';

INSERT INTO `es_user` VALUES
(1, 0, 0, 0, 0, '127.0.0.1', 'admin', 'admin', '0dfc7612f607db6c17fd99388e9e5f9c', '/public/images/default.png', 'admin@admin.com', '', '1496145982', 0, 0, 1, NULL, NULL, NULL, '1500629119', '127.0.0.1', '1dFlxLhiuLqnUZe9kA', '0', '0', '1');





DROP TABLE IF EXISTS `es_auth_group`;
CREATE TABLE `es_auth_group` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '用户组id,自增主键',
  `module` varchar(20) NOT NULL DEFAULT '' COMMENT '用户组所属模块',
  `name` char(30) NOT NULL DEFAULT '' COMMENT '用户组名称',
  `describe` varchar(80) NOT NULL DEFAULT '' COMMENT '描述信息',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '用户组状态：为1正常，为0禁用,-1为删除',
  `rules` varchar(1000) NOT NULL DEFAULT '' COMMENT '用户组拥有的规则id，多个规则 , 隔开',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='权限组表';


DROP TABLE IF EXISTS `es_auth_group_access`;
CREATE TABLE `es_auth_group_access` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '用户组id,自增主键',
  `member_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '用户id',
  `group_id` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '用户组id',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5  DEFAULT CHARSET=utf8 COMMENT='用户组授权表';




DROP TABLE IF EXISTS `es_config`;
CREATE TABLE `es_config` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '配置ID',
  `name` varchar(30) NOT NULL DEFAULT '' COMMENT '配置名称',
  `type` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '配置类型',
  `title` varchar(50) NOT NULL DEFAULT '' COMMENT '配置标题',
  `group` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '配置分组',
  `extra` varchar(255) NOT NULL DEFAULT '' COMMENT '配置选项',
  `describe` varchar(255) NOT NULL DEFAULT '' COMMENT '配置说明',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态',
  `value` text NOT NULL COMMENT '配置值',
  `sort` smallint(3) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_name` (`name`),
  KEY `type` (`type`),
  KEY `group` (`group`)
) ENGINE=MyISAM AUTO_INCREMENT=1024 DEFAULT CHARSET=utf8;

INSERT INTO `es_config` VALUES(1, 'WEB_SITE_TITLE', 1, '网站标题', 1, '', '网站标题前台显示标题', 1378898976, 1492276847, 1, 'ESPHP框架', 3);
INSERT INTO `es_config` VALUES(2, 'WEB_SITE_DESCRIPTION', 2, '网站描述', 1, '', '网站搜索引擎描述', 1378898976, 1492276843, 1, '', 100);
INSERT INTO `es_config` VALUES(3, 'WEB_SITE_KEYWORD', 2, '网站关键字', 1, '', '网站搜索引擎关键字', 1378898976, 1492276839, 1, 'ESPHP框架', 99);
INSERT INTO `es_config` VALUES(4, 'WEB_SITE_CLOSE', 4, '关闭站点', 1, '0:关闭,1:开启', '站点关闭后其他用户不能访问，管理员可以正常访问', 1378898976, 1492273146, 1, '1', 1);
INSERT INTO `es_config` VALUES(5, 'SYS_VERSION', 1, '版本号', 0, '', '系统版本号', 1378898976, 1492276847, 1, '1.0', 3);
INSERT INTO `es_config` VALUES(9, 'config_type_list', 3, '配置类型列表', 3, '', '主要用于数据解析和页面表单的生成', 1378898976, 1492275785, 1, '0:数字\r\n1:字符\r\n2:文本\r\n3:数组\r\n4:枚举', 100);
INSERT INTO `es_config` VALUES(10, 'WEB_SITE_ICP', 1, '网站备案号', 1, '', '设置在网站底部显示的备案号，如“沪ICP备12007941号-2', 1378900335, 1492276833, 1, 'asdas', 9);
INSERT INTO `es_config` VALUES(20, 'config_group_list', 3, '配置分组', 3, '', '配置分组', 1379228036, 1492275841, 1, '1:基础\r\n2:数据\r\n3:系统\r\n4:扩展', 100);
INSERT INTO `es_config` VALUES(21, 'HOOKS_TYPE', 3, '钩子的类型', 4, '', '类型 1-用于扩展显示内容，2-用于扩展业务处理', 1379313397, 1492272816, -1, '1:视图\r\n2:控制器', 100);
INSERT INTO `es_config` VALUES(22, 'AUTH_CONFIG', 3, 'Auth配置', 4, '', '自定义Auth.class.php类配置', 1379409310, 1492272794, -1, 'AUTH_ON:1\r\nAUTH_TYPE:2', 100);
INSERT INTO `es_config` VALUES(23, 'OPEN_DRAFTBOX', 4, '是否开启草稿功能', 2, '0:关闭草稿功能\r\n1:开启草稿功能\r\n', '新增文章时的草稿功能配置', 1379484332, 1492272799, 1, '1', 2);
INSERT INTO `es_config` VALUES(24, 'DRAFT_AOTOSAVE_INTERVAL', 1, '自动保存草稿时间', 2, '', '自动保存草稿的时间间隔，单位：秒', 1379484574, 1492272804, 0, '60', 3);
INSERT INTO `es_config` VALUES(25, 'list_rows', 0, '每页数据记录数', 2, '', '数据每页显示记录数', 1379503896, 1492276739, 1, '10', 1);
INSERT INTO `es_config` VALUES(26, 'USER_ALLOW_REGISTER', 4, '是否允许用户注册', 1, '0:关闭注册\r\n1:允许注册', '是否开放用户注册', 1379504487, 1492272822, 1, '1', 3);
INSERT INTO `es_config` VALUES(28, 'DATA_BACKUP_PATH', 1, '数据库备份根路径', 4, '', '路径必须以 / 结尾', 1381482411, 1492273011, 1, './data/', 5);
INSERT INTO `es_config` VALUES(29, 'DATA_BACKUP_PART_SIZE', 1, '数据库备份卷大小', 4, '', '该值用于限制压缩后的分卷最大长度。单位：B；建议设置20M', 1381482488, 1492272902, 1, '20971520', 7);
INSERT INTO `es_config` VALUES(30, 'DATA_BACKUP_COMPRESS', 4, '数据库备份文件是否启用压缩', 4, '0:不压缩\r\n1:启用压缩', '压缩备份文件需要PHP环境支持\r\n\r\ngzopen,gzwrite函数', 1381713345, 1492272906, 1, '1', 9);
INSERT INTO `es_config` VALUES(31, 'DATA_BACKUP_COMPRESS_LEVEL', 4, '数据库备份文件压缩级别', 4, '1:普通\r\n4:一般\r\n9:最高', '数据库备份文件的压缩级别，该配置\r\n\r\n在开启压缩时生效', 1381713408, 1492272910, 1, '9', 10);
INSERT INTO `es_config` VALUES(32, 'OPEN_ROUTER', 4, '开启路由模式', 3, '0:关闭\r\n1:开启', '是否开启路由模式', 1383105995, 1492272922, 1, '1', 11);
INSERT INTO `es_config` VALUES(33, 'allow_url', 3, '不受权限验证的url', 3, '', '', 1386644047, 1492276816, 1, '0:file/pictureupload\r\n1:addon/execute\r\n2:index/adminindex\r\n3:index/home\r\n4:user/changePass', 100);
INSERT INTO `es_config` VALUES(34, 'DENY_VISIT', 3, '超管专限控制器方法', 1, '', '仅超级管理员可访问的控制器方法', 1386644141, 1492272998, -1, '0:Addons/addhook\r\n1:Addons/edithook\r\n2:Addons/delhook\r\n3:Addons/updateHook', 100);
INSERT INTO `es_config` VALUES(35, 'REPLY_LIST_ROWS', 1, '回复列表每页条数', 2, '', '', 1386645376, 1492273230, -1, '10', 0);
INSERT INTO `es_config` VALUES(36, 'ADMIN_ALLOW_IP', 2, '后台允许访问IP', 3, '', '多个用逗号分隔，如果不配置表示不限制IP访问', 1387165454, 1492276749, 1, '', 12);
INSERT INTO `es_config` VALUES(43, 'empty_list_describe', 1, '数据列表为空时的描述信息', 2, '', '', 1492278127, 1492278127, 1, 'aOh! 暂时还没有数据~', 0);
INSERT INTO `es_config` VALUES(44, 'trash_config', 3, '回收站配置', 3, '', 'key为模型名称，值为显示列。', 1492312698, 1492925148, 1, 'Config:name\r\nAuthGroup:name\r\nUser:nickname\r\nMenu:name\r\nAddon:name\r\nPicture:name', 0);
INSERT INTO `es_config` VALUES(45, 'yzm_list', 3, '验证码配置', 3, '', '1注册2登录3忘记密码4后台登录', 1378898976, 1492275785, 1, '1\r\n2\r\n3\r\n4', 100);
INSERT INTO `es_config` VALUES(46, 'Cache_open', 4, '是否开启缓存机制', 2, '0:关闭缓存\r\n1:开启缓存\r\n', '缓存开关', 1379484332, 1492272799, 1, '0', 4);

INSERT INTO `es_config` VALUES ('47', 'cache_max_number', '1', '缓存最大数量', '2', '', '允许缓存的最大数量', '1494222635', '1502504402', '1', '1000', '5');
INSERT INTO `es_config` VALUES ('48', 'cache_clear_interval_time', '1', '缓存回收时间', '2', '', '缓存时间以秒计算', '1494222635', '1502504402', '1', '600', '6');
INSERT INTO `es_config` VALUES ('50', 'site_tpl', '1', '前台模板', '1', '', '模板名称', '1494222635', '1502504402', '1', 'simple', '0');
INSERT INTO `es_config` VALUES(51, 'WEB_SITE_FOOTER', 2, '底部代码', 1, '', '可以添加统计代码等', 1378898976, 1492276843, 1, '', 100);

INSERT INTO `es_config` VALUES ('52', 'upload_file_ext', '1', '附件上传后缀限制', '3', '', '多个后缀名请用半角逗号隔开', '1494222635', '1502504402', '1', 'doc', '0');
INSERT INTO `es_config` VALUES ('53', 'upload_file_size', '1', '附件上传大小限制', '3', '', '单位为字节', '1494222635', '1502504402', '1', '2048000', '0');

INSERT INTO `es_config` VALUES(54, 'point_type_list', 3, '积分规则类型列表', 3, '', '主要用于积分规则的管理', 1378898976, 1492275785, 1, 'login:登录', 100);
INSERT INTO `es_config` VALUES(55, 'keyword_list', 3, '关键词列表', 3, '', '一行一个', 1378898976, 1492275785, 1, '学习\r\n讲话', 100);

INSERT INTO `es_config` VALUES ('56', 'storage_driver', '1', '存储驱动', '2', '', '若无需使用云存储则为空', '1494222635', '1502504402', '1', '', '5');


INSERT INTO `es_config` VALUES(57, 'scoretype_list', 3, '积分类型列表', 3, '', '一行一个', 1378898976, 1492275785, 1, 'point:财富值\r\nexpoint1:经验值', 100);

INSERT INTO `es_config` VALUES ('58', 'upload_picture_ext', '1', '图片上传后缀限制', '3', '', '多个后缀名请用半角逗号隔开', '1494222635', '1502504402', '1', 'jpg,gif,png', '0');
INSERT INTO `es_config` VALUES ('59', 'upload_picture_size', '1', '图片上传大小限制', '3', '', '单位为字节', '1494222635', '1502504402', '1', '2048000', '0');
INSERT INTO `es_config` VALUES ('60', 'web_url', '1', '网站域名', '1', '', '含http://,如果是子目录也需要填写', '1494222635', '1502504402', '1', 'http://es.imzaker.com/zaker/', '0');


INSERT INTO `es_config` VALUES ('61', 'jwt_key', '1', 'JWT加密KEY', '4', '', '', '1506748805', '1509432155', '1', 'l2V|DSFXXXgfZp{8`;FjzR~6Y1_', '0');
INSERT INTO `es_config` VALUES ('62', 'api_status_option', '3', 'API接口状态', '4', '', '', '1504242433', '1509432155', '1', '0:待研发\r\n1:研发中\r\n2:测试中\r\n3:已完成', '0');
INSERT INTO `es_config` VALUES ('63', 'api_data_type_option', '3', 'API数据类型', '4', '', '', '1504328208', '1509432155', '1', '0:字符\r\n1:文本\r\n2:数组\r\n3:文件', '0');
INSERT INTO `es_config` VALUES ('64', 'api_domain', '1', 'API部署域名', '4', '', '', '1504779094', '1509432155', '1', 'https://www.imzaker.com', '0');
INSERT INTO `es_config` VALUES ('65', 'api_key', '1', 'API加密KEY', '4', '', '泄露后API将存在安全隐患', '1505302112', '1509432155', '1', 'l2V|gfZp{8`;jzR~6Y1_', '0');
INSERT INTO `es_config` VALUES ('66', 'team_developer', '3', '研发团队人员', '4', '', '', '1504236453', '1509432155', '1', '0:Zaker\r\n1:Sun', '0');

INSERT INTO `es_config` VALUES(67, 'is_write_exe_log', 4, '执行信息写入', 3, '0:关闭\r\n1:开启', '是否开启执行信息写入', 1383105995, 1492272922, 1, '1', 11);




INSERT INTO `es_config` VALUES ('69', 'mailport', '1', '邮箱端口', '4', '', '根据不同邮箱介绍进行设置', '1494222635', '1502504402', '1', '587', '0');


INSERT INTO `es_config` VALUES ('70', 'mailusername', '1', '邮箱用户名', '4', '', '邮箱用户名', '1494222635', '1502504402', '1', '', '0');

INSERT INTO `es_config` VALUES ('71', 'mailpassword', '1', '邮箱密码', '4', '', '邮箱密码', '1494222635', '1502504402', '1', '', '0');

INSERT INTO `es_config` VALUES ('72', 'mailname', '1', '发信签名', '4', '', '发信时签名', '1494222635', '1502504402', '1', 'ESPHP', '0');

INSERT INTO `es_config` VALUES ('73', 'mailserver', '1', '邮箱服务器', '4', '', '邮箱服务器', '1494222635', '1502504402', '1', 'smtp.qq.com', '0');


DROP TABLE IF EXISTS `es_menu`;
CREATE TABLE `es_menu` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '文档ID',
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '菜单名称',
  `pid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '上级分类ID',
  `sort` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '排序（同级有效）',
  `module` char(20) NOT NULL DEFAULT '' COMMENT '模块',
  `url` char(255) NOT NULL DEFAULT '' COMMENT '链接地址',
  `is_hide` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否隐藏',
  `icon` char(30) NOT NULL DEFAULT '' COMMENT '图标',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10240 DEFAULT CHARSET=utf8 COMMENT='菜单表';

-- ----------------------------
-- Records of es_menu
-- ----------------------------

INSERT INTO `es_menu` VALUES ('1', '系统管理', '0', '4', 'admin', 'config/index', '0', 'fa-wrench', '1', '1491578008', '9');
INSERT INTO `es_menu` VALUES ('2', '内容管理', '0', '6', 'admin', 'content/index', '0', 'fa-newspaper-o', '1', '1491578008', '9');
INSERT INTO `es_menu` VALUES ('3', '用户管理', '0', '5', 'admin', 'user/index', '0', 'fa-user', '1', '1491837091', '1');
INSERT INTO `es_menu` VALUES ('4', '扩展管理', '0', '1', 'admin', 'extend/index', '0', 'fa-object-group', '1', '1491837091', '1');




INSERT INTO `es_menu` VALUES ('5', '插件列表', '4', '3', 'admin', 'addon/addonlist', '0', 'fa-microchip', '1', '1491837214', '0');
INSERT INTO `es_menu` VALUES ('6', '钩子列表', '4', '2', 'admin', 'addon/hooklist', '0', 'fa-anchor', '1', '1492000451', '0');
INSERT INTO `es_menu` VALUES ('7', '服务列表', '4', '1', 'admin', 'service/servicelist', '0', 'fa-server', '1', '1491837214', '0');
INSERT INTO `es_menu` VALUES ('8', '菜单列表', '1', '5', 'admin', 'menu/menulist', '0', 'fa-asl-interpreting', '1', '1491318724', '0');






INSERT INTO `es_menu` VALUES ('9', '回收站', '1', '2', 'admin', 'trash/trashlist', '0', 'fa-recycle', '1', '1492320214', '1492311462');
INSERT INTO `es_menu` VALUES ('901', '数据列表', '9', '1', 'admin', 'trash/trashdatalist', '0', 'fa-inbox', '1', '1491318724', '0');
INSERT INTO `es_menu` VALUES ('902', '数据删除', '9', '1', 'admin', 'trash/trashdatadel', '0', 'fa-inbox', '1', '1491318724', '0');
INSERT INTO `es_menu` VALUES ('903', '数据恢复', '9', '1', 'admin', 'trash/restoredata', '0', 'fa-inbox', '1', '1491318724', '0');


INSERT INTO `es_menu` VALUES ('10', '备份数据', '1', '1', 'admin', 'database/databaselist', '0', 'fa-inbox', '1', '1491318724', '0');
INSERT INTO `es_menu` VALUES ('1001', '备份数据', '10', '1', 'admin', 'database/export', '0', 'fa-inbox', '1', '1491318724', '0');
INSERT INTO `es_menu` VALUES ('1002', '优化表', '10', '1', 'admin', 'database/optimize', '0', 'fa-inbox', '1', '1491318724', '0');
INSERT INTO `es_menu` VALUES ('1003', '修复表', '10', '1', 'admin', 'database/repair', '0', 'fa-inbox', '1', '1491318724', '0');



INSERT INTO `es_menu` VALUES ('11', '还原数据', '1', '0', 'admin', 'database/importlist', '0', 'fa-undo', '1', '1491318724', '0');
INSERT INTO `es_menu` VALUES ('1101', '还原数据库', '11', '0', 'admin', 'database/import', '0', 'fa-undo', '1', '1491318724', '0');
INSERT INTO `es_menu` VALUES ('1102', '删除数据库', '11', '0', 'admin', 'database/delete', '0', 'fa-undo', '1', '1491318724', '0');
INSERT INTO `es_menu` VALUES ('1103', '下载备份', '11', '0', 'admin', 'database/download', '0', 'fa-undo', '1', '1491318724', '0');



INSERT INTO `es_menu` VALUES ('12', '配置管理', '1', '4', 'admin', 'config/configlist', '0', 'fa-gears', '1', '1491668183', '0');
INSERT INTO `es_menu` VALUES ('1201', '系统配置', '1', '6', 'admin', 'config/setting', '0', 'fa-gear', '1', '1491668183', '0');
INSERT INTO `es_menu` VALUES ('1202', '配置编辑', '12', '0', 'admin', 'config/configedit', '1', '', '1', '1491674180', '0');
INSERT INTO `es_menu` VALUES ('1203', '配置删除', '12', '0', 'admin', 'config/configdel', '1', '', '1', '1491674201', '0');
INSERT INTO `es_menu` VALUES ('1204', '配置添加', '12', '0', 'admin', 'config/configadd', '0', 'fa-plus', '1', '1491666947', '0');
INSERT INTO `es_menu` VALUES ('1205', '配置批量删除', '12', '0', 'admin', 'config/configalldel', '1', '', '1', '1491674201', '0');


INSERT INTO `es_menu` VALUES ('13', '会员列表', '3', '3', 'admin', 'user/memberlist', '0', 'fa-id-card-o', '1', '1491837214', '0');
INSERT INTO `es_menu` VALUES ('1301', '会员添加', '13', '3', 'admin', 'user/memberadd', '1', 'fa-id-card-o', '1', '1491837214', '0');
INSERT INTO `es_menu` VALUES ('1302', '会员编辑', '13', '3', 'admin', 'user/memberedit', '1', 'fa-id-card-o', '1', '1491837214', '0');
INSERT INTO `es_menu` VALUES ('1303', '会员批量删除', '13', '3', 'admin', 'user/memberalldel', '1', 'fa-id-card-o', '1', '1491837214', '0');
INSERT INTO `es_menu` VALUES ('1304', '会员删除', '13', '3', 'admin', 'user/memberdel', '1', 'fa-id-card-o', '1', '1491837214', '0');
INSERT INTO `es_menu` VALUES ('1305', '会员授权', '13', '3', 'admin', 'user/memberauth', '1', 'fa-id-card-o', '1', '1491837214', '0');
INSERT INTO `es_menu` VALUES ('1306', '会员认证', '13', '3', 'admin', 'user/memberrz', '1', 'fa-id-card-o', '1', '1491837214', '0');

INSERT INTO `es_menu` VALUES ('14', '会员等级', '3', '2', 'admin', 'usergrade/usergradelist', '0', 'fa-signal', '1', '1491837214', '0');
INSERT INTO `es_menu` VALUES ('1401', '会员等级添加', '14', '3', 'admin', 'usergrade/usergradeadd', '1', 'fa-id-card-o', '1', '1491837214', '0');
INSERT INTO `es_menu` VALUES ('1402', '会员等级编辑', '14', '3', 'admin', 'usergrade/usergradeedit', '1', 'fa-id-card-o', '1', '1491837214', '0');
INSERT INTO `es_menu` VALUES ('1403', '会员等级批量删除', '14', '3', 'admin', 'usergrade/usergradealldel', '1', 'fa-id-card-o', '1', '1491837214', '0');
INSERT INTO `es_menu` VALUES ('1404', '会员等级删除', '14', '3', 'admin', 'usergrade/usergradedel', '1', 'fa-id-card-o', '1', '1491837214', '0');
INSERT INTO `es_menu` VALUES ('1405', '会员等级授权', '14', '3', 'admin', 'usergrade/usergradeauth', '1', 'fa-id-card-o', '1', '1491837214', '0');


INSERT INTO `es_menu` VALUES ('15', '权限管理', '3', '1', 'admin', 'auth/authgrouplist', '0', 'fa-suitcase', '1', '1492000451', '0');
INSERT INTO `es_menu` VALUES ('1501', '权限组添加', '15', '3', 'admin', 'auth/authgroupadd', '1', 'fa-id-card-o', '1', '1491837214', '0');
INSERT INTO `es_menu` VALUES ('1502', '权限组编辑', '15', '3', 'admin', 'auth/authgroupedit', '1', 'fa-id-card-o', '1', '1491837214', '0');
INSERT INTO `es_menu` VALUES ('1503', '权限组批量删除', '15', '3', 'admin', 'auth/authgroupalldel', '1', 'fa-id-card-o', '1', '1491837214', '0');
INSERT INTO `es_menu` VALUES ('1504', '权限组删除', '15', '3', 'admin', 'auth/authgroupdel', '1', 'fa-id-card-o', '1', '1491837214', '0');
INSERT INTO `es_menu` VALUES ('1505', '权限组授权', '15', '3', 'admin', 'auth/authmenuauth', '1', 'fa-id-card-o', '1', '1491837214', '0');


INSERT INTO `es_menu` VALUES ('16', '前台导航', '1', '4', 'admin', 'nav/navlist', '0', 'fa-life-bouy', '1', '1491668183', '0');
INSERT INTO `es_menu` VALUES ('1601', '导航添加', '16', '3', 'admin', 'nav/navadd', '1', 'fa-id-card-o', '1', '1491837214', '0');
INSERT INTO `es_menu` VALUES ('1602', '导航编辑', '16', '3', 'admin', 'nav/navedit', '1', 'fa-id-card-o', '1', '1491837214', '0');
INSERT INTO `es_menu` VALUES ('1603', '导航批量删除', '16', '3', 'admin', 'nav/navalldel', '1', 'fa-id-card-o', '1', '1491837214', '0');
INSERT INTO `es_menu` VALUES ('1604', '导航删除', '16', '3', 'admin', 'nav/navdel', '1', 'fa-id-card-o', '1', '1491837214', '0');
INSERT INTO `es_menu` VALUES ('1605', '导航状态更新', '16', '3', 'admin', 'nav/navcstatus', '1', 'fa-id-card-o', '1', '1491837214', '0');

INSERT INTO `es_menu` VALUES ('17', '评论数据', '2', '3', 'admin', 'comment/commentlist', '0', 'fa-comments-o', '1', '1491318724', '0');
INSERT INTO `es_menu` VALUES ('1701', '评论编辑', '17', '3', 'admin', 'comment/commentedit', '1', 'fa-id-card-o', '1', '1491837214', '0');
INSERT INTO `es_menu` VALUES ('1702', '评论批量删除', '17', '3', 'admin', 'comment/commentalldel', '1', 'fa-id-card-o', '1', '1491837214', '0');
INSERT INTO `es_menu` VALUES ('1703', '评论删除', '17', '3', 'admin', 'comment/commentdel', '1', 'fa-id-card-o', '1', '1491837214', '0');



INSERT INTO `es_menu` VALUES ('18', '消息列表', '2', '2', 'admin', 'message/messagelist', '0', 'fa-volume-up', '1', '1491318724', '0');
INSERT INTO `es_menu` VALUES ('1801', '消息添加', '18', '3', 'admin', 'message/messageadd', '1', 'fa-id-card-o', '1', '1491837214', '0');
INSERT INTO `es_menu` VALUES ('1802', '消息编辑', '18', '3', 'admin', 'message/messageedit', '1', 'fa-id-card-o', '1', '1491837214', '0');
INSERT INTO `es_menu` VALUES ('1803', '消息批量删除', '18', '3', 'admin', 'message/messagealldel', '1', 'fa-id-card-o', '1', '1491837214', '0');
INSERT INTO `es_menu` VALUES ('1804', '消息删除', '18', '3', 'admin', 'message/messagedel', '1', 'fa-id-card-o', '1', '1491837214', '0');


INSERT INTO `es_menu` VALUES ('19', '积分规则', '1', '3', 'admin', 'pointrule/pointrulelist', '0', 'fa-eercast', '1', '1491837214', '0');
INSERT INTO `es_menu` VALUES ('1901', '规则添加', '19', '3', 'admin', 'pointrule/pointruleadd', '1', 'fa-id-card-o', '1', '1491837214', '0');
INSERT INTO `es_menu` VALUES ('1902', '规则编辑', '19', '3', 'admin', 'pointrule/pointruleedit', '1', 'fa-id-card-o', '1', '1491837214', '0');
INSERT INTO `es_menu` VALUES ('1903', '规则批量删除', '19', '3', 'admin', 'pointrule/pointrulealldel', '1', 'fa-id-card-o', '1', '1491837214', '0');
INSERT INTO `es_menu` VALUES ('1904', '规则删除', '19', '3', 'admin', 'pointrule/pointruledel', '1', 'fa-id-card-o', '1', '1491837214', '0');


INSERT INTO `es_menu` VALUES ('25', '文章分类', '2', '5', 'admin', 'articlecate/articlecatelist', '0', 'fa-puzzle-piece', '1', '1491318724', '0');
INSERT INTO `es_menu` VALUES ('2501', '文章分类添加', '25', '3', 'admin', 'articlecate/articlecateadd', '1', 'fa-id-card-o', '1', '1491837214', '0');
INSERT INTO `es_menu` VALUES ('2502', '文章分类编辑', '25', '3', 'admin', 'articlecate/articlecateedit', '1', 'fa-id-card-o', '1', '1491837214', '0');
INSERT INTO `es_menu` VALUES ('2503', '文章分类批量删除', '25', '3', 'admin', 'articlecate/articlecatealldel', '1', 'fa-id-card-o', '1', '1491837214', '0');
INSERT INTO `es_menu` VALUES ('2504', '文章分类删除', '25', '3', 'admin', 'articlecate/articlecatedel', '1', 'fa-id-card-o', '1', '1491837214', '0');


INSERT INTO `es_menu` VALUES ('26', '文章管理', '2', '5', 'admin', 'article/articlelist', '0', 'fa-sticky-note-o', '1', '1491318724', '0');
INSERT INTO `es_menu` VALUES ('2601', '文章添加', '26', '3', 'admin', 'article/articleadd', '1', 'fa-id-card-o', '1', '1491837214', '0');
INSERT INTO `es_menu` VALUES ('2602', '文章编辑', '26', '3', 'admin', 'article/articleedit', '1', 'fa-id-card-o', '1', '1491837214', '0');
INSERT INTO `es_menu` VALUES ('2603', '文章批量删除', '26', '3', 'admin', 'article/articlealldel', '1', 'fa-id-card-o', '1', '1491837214', '0');
INSERT INTO `es_menu` VALUES ('2604', '文章删除', '26', '3', 'admin', 'article/articledel', '1', 'fa-id-card-o', '1', '1491837214', '0');
INSERT INTO `es_menu` VALUES ('2605', '文章状态更新', '26', '3', 'admin', 'article/articlecstatus', '1', 'fa-id-card-o', '1', '1491837214', '0');



INSERT INTO `es_menu` VALUES ('27', '接口管理', '0', '2', 'admin', 'api/index', '0', 'fa-book', '1', '1504000462', '1504000434');
INSERT INTO `es_menu` VALUES ('2701', '分组管理', '27', '0', 'admin', 'api/apigrouplist', '0', 'fa-fw fa-th-list', '1', '1504000977', '1504000723');
INSERT INTO `es_menu` VALUES ('2702', '分组添加', '27', '0', 'admin', 'api/apigroupadd', '1', 'fa-fw fa-plus', '1', '1504004646', '1504004646');
INSERT INTO `es_menu` VALUES ('2703', '分组编辑', '27', '0', 'admin', 'api/apigroupedit', '1', '', '1', '1504004710', '1504004710');
INSERT INTO `es_menu` VALUES ('2704', '分组删除', '27', '0', 'admin', 'api/apigroupdel', '1', '', '1', '1504004732', '1504004732');
INSERT INTO `es_menu` VALUES ('2705', '接口列表', '27', '0', 'admin', 'api/apilist', '0', 'fa-fw fa-th-list', '1', '1504172326', '1504172326');
INSERT INTO `es_menu` VALUES ('2706', '接口添加', '27', '0', 'admin', 'api/apiadd', '1', 'fa-fw fa-plus', '1', '1504172352', '1504172352');
INSERT INTO `es_menu` VALUES ('2707', '接口编辑', '27', '0', 'admin', 'api/apiedit', '1', '', '1', '1504172414', '1504172414');
INSERT INTO `es_menu` VALUES ('2708', '接口删除', '27', '0', 'admin', 'api/apidel', '1', '', '1', '1504172435', '1504172435');



INSERT INTO `es_menu` VALUES ('28', '全局范围', '1', '0', 'admin', 'exelog/applist', '0', 'fa-tags', '1', '1509433570', '1509433570');
INSERT INTO `es_menu` VALUES ('29', '接口范围', '1', '0', 'admin', 'exelog/apilist', '0', 'fa-tag', '1', '1509433591', '1509433591');


INSERT INTO `es_menu` VALUES ('31', '积分记录', '1', '3', 'admin', 'pointnote/pointnoteList', '0', 'fa-eercast', '1', '1491837214', '0');
INSERT INTO `es_menu` VALUES ('3101', '记录添加', '31', '3', 'admin', 'pointnote/pointnoteAdd', '1', 'fa-id-card-o', '1', '1491837214', '0');
INSERT INTO `es_menu` VALUES ('3102', '记录编辑', '31', '3', 'admin', 'pointnote/pointnoteEdit', '1', 'fa-id-card-o', '1', '1491837214', '0');
INSERT INTO `es_menu` VALUES ('3103', '记录批量删除', '31', '3', 'admin', 'pointnote/pointnoteAlldel', '1', 'fa-id-card-o', '1', '1491837214', '0');
INSERT INTO `es_menu` VALUES ('3104', '记录删除', '31', '3', 'admin', 'pointnote/pointnoteDel', '1', 'fa-id-card-o', '1', '1491837214', '0');

DROP TABLE IF EXISTS `es_exe_log`;
CREATE TABLE `es_exe_log` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键自增',
  `ip` char(50) NOT NULL DEFAULT '' COMMENT 'IP地址',
  `exe_url` varchar(2000) NOT NULL DEFAULT '' COMMENT '执行URL',
  `exe_time` float(10,6) unsigned NOT NULL DEFAULT '0.000000' COMMENT '执行时间 单位 秒',
  `exe_memory` char(20) NOT NULL DEFAULT '0.00' COMMENT '内存占用KB',
  `exe_os` char(30) NOT NULL DEFAULT '' COMMENT '操作系统',
  `source_url` varchar(2000) NOT NULL DEFAULT '' COMMENT '来源URL',
  `session_id` char(32) NOT NULL DEFAULT '' COMMENT 'session_id',
  `browser` char(30) NOT NULL DEFAULT '' COMMENT '浏览器',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0',
  `login_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '执行者ID',
  `type` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '类型  0 ： 应用范围 ， 1：API 范围 ',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=235 DEFAULT CHARSET=utf8 COMMENT='执行记录表';

DROP TABLE IF EXISTS `es_article`;
CREATE TABLE IF NOT EXISTS `es_article` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tid` int(11) NOT NULL COMMENT '上级',
  `uid` int(11) NOT NULL COMMENT '用户',
  `title` varchar(100) NOT NULL COMMENT '标题',
  `choice` tinyint(1) NOT NULL DEFAULT '0' COMMENT '精贴',
  `settop` tinyint(1) NOT NULL DEFAULT '0' COMMENT '顶置',
  `praise` int(11) NOT NULL DEFAULT '0' COMMENT '赞',
  `view` int(11) NOT NULL DEFAULT '0' COMMENT '浏览量',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0',
  `keywords` varchar(100) NOT NULL COMMENT '关键词',
  `description` varchar(200) NOT NULL COMMENT '描述',
  `content` text NOT NULL COMMENT '内容',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `es_articlecate`;
CREATE TABLE IF NOT EXISTS `es_articlecate` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '分类ID',
  `pid` int(11) NOT NULL DEFAULT '0' COMMENT '父ID',
  `name` varchar(30) NOT NULL DEFAULT '' COMMENT '分类名称',
  `describe` varchar(255) NOT NULL DEFAULT '' COMMENT '描述',
  `pidstr` varchar(30) NOT NULL DEFAULT '' COMMENT '分类字符串',
  `cover_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '封面图片id',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '数据状态',
  `sort` int(11) NOT NULL DEFAULT '0' COMMENT '排序',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='文章分类表';



DROP TABLE IF EXISTS `es_comment`;
CREATE TABLE IF NOT EXISTS `es_comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tid` int(11) NOT NULL COMMENT '上级评论',
  `uid` int(11) NOT NULL COMMENT '所属会员',
  `fid` int(11) NOT NULL COMMENT '所属帖子',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0',
  `praise` int(11) DEFAULT '0' COMMENT '赞',
  `reply` int(11) DEFAULT '0' COMMENT '回复',
  `content` text NOT NULL COMMENT '内容',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='评论表';




DROP TABLE IF EXISTS `es_message`;
CREATE TABLE IF NOT EXISTS `es_message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL COMMENT '所属会员',
  `touid` int(11) NOT NULL DEFAULT '0' COMMENT '发送对象',
  `type` tinyint(3) NOT NULL DEFAULT '1' COMMENT '1系统消息2帖子动态',
  `content` text NOT NULL COMMENT '内容',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态2表示已读',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='消息表';

DROP TABLE IF EXISTS `es_readmessage`;
CREATE TABLE IF NOT EXISTS `es_readmessage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL COMMENT '会员',
  `mid` int(11) NOT NULL DEFAULT '0' COMMENT '消息id',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='消息已读表';

DROP TABLE IF EXISTS `es_readtime`;
CREATE TABLE IF NOT EXISTS `es_readtime` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL COMMENT '会员',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态2表示已读',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='用户消息时间';

DROP TABLE IF EXISTS `es_nav`;
CREATE TABLE `es_nav` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` tinyint(3) unsigned NOT NULL COMMENT '顶部还是底部',
  `sid` tinyint(3) unsigned NOT NULL COMMENT '内部还是外部',
  `name` varchar(20) NOT NULL COMMENT '导航名称',
  `alias` varchar(20) DEFAULT '' COMMENT '导航别称',
  `link` varchar(255) DEFAULT '' COMMENT '导航链接',
  `icon` varchar(255) DEFAULT '' COMMENT '导航图标',
  `target` varchar(10) DEFAULT '' COMMENT '打开方式',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态',
  `sort` int(11) NOT NULL DEFAULT '0' COMMENT '排序',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='导航表';

INSERT INTO `es_nav` VALUES
(1, 1, 1, '首页', 'Home', 'Index/index', 'Home', '_blank', 1, 0, 1507709105, 0);


DROP TABLE IF EXISTS `es_zan`;
CREATE TABLE `es_zan` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) unsigned NOT NULL COMMENT '用户',
  `sid` int(11) unsigned NOT NULL COMMENT '相关内容id',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `type` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '类型自定义数字含义',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='用户操作表';


DROP TABLE IF EXISTS `es_point_note`;
CREATE TABLE `es_point_note` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `controller` varchar(255) NOT NULL,
  `uid` int(10) unsigned NOT NULL,
  `type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1为增加2为减少',
  `score` int(10) NOT NULL,
  `itemid` varchar(11) NOT NULL DEFAULT '0' COMMENT '表示帖子或者其他各种类型的主键id',
  `infouid` varchar(11) NOT NULL DEFAULT '0' COMMENT '规则id或者受益人uid',
  `scoretype` varchar(30) NOT NULL DEFAULT '' COMMENT '积分类型',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '数据状态',
  PRIMARY KEY (`id`)
)ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `es_point_rule`;
CREATE TABLE IF NOT EXISTS `es_point_rule` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '规则ID',
  `controller` varchar(30) NOT NULL DEFAULT '' COMMENT '规则名称',
  `type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1为增加2为减少',
  `score` varchar(11) NOT NULL DEFAULT '0' COMMENT '积分',
  `num` varchar(11) NOT NULL DEFAULT '0' COMMENT '二十四小时奖赏次数',
  `scoretype` varchar(30) NOT NULL DEFAULT '' COMMENT '积分类型',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '数据状态',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='积分规则表';




DROP TABLE IF EXISTS `es_searchword`;
CREATE TABLE IF NOT EXISTS `es_searchword` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(100) NOT NULL DEFAULT '' COMMENT '热搜关键词',
  `uid` int(10) NOT NULL,
  `num` int(20) NOT NULL DEFAULT '1' COMMENT '搜索次数',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '数据状态',
  `sort` int(11) NOT NULL DEFAULT '0' COMMENT '排序',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='热搜表';
